wp-slicypay
===========

This is the project directory for the wp-slicypay module.
The module itself is in the ``src`` directory, this higher directory
houses some aditional development related bits that are kept separate
from the main module so that WordPress conventions can be more clearly
maintained in the module itself while not contstrain some development
choices.
