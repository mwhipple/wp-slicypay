=== wp-slicypay ===

Contributors: Matt Whipple <matt@mattwhipple.com>
Requires at least: 3.2
Tested up to: 4.3
License: Apache-2.0
License URI: http://www.apache.org/licenses/LICENSE-2.0

Lightweight WordPress support for CyberSource hosted Secure Acceptance payment.
SLIm CYberSource PAYment module

Overview
========

This plugin is intended to be lightweight glue code rather than comprehensive
functionality. It primarily serves to create an interface that is a better fit for
simple payment screents rather than the more ecommerce oriented flow which
CyberSource supports by default (i.e. the user needs to pay an explicit amount
rather than an amount being calculated as a result of a shopping cart style
selection of offerings).

To do this it collects the additional business logic in a local form,
and then uses an AJAX request to sign that data and initiate the
conversation with CyberSource. This allows for a secure way of getting
the user more quickly to the payment screen (bypassing a longer workflow
with confirmation).

Included below are two brief user guides:

* The _Administrator Guide_ covers maintenance that will need to be done regularly
on the site without impacting the way the module functions.
* The _Author Guide_ includes information about how to integrate the module into
the site content

There is also some notes about the plugin's _Original Implementation_ in regards
to how that influenced the current design.

Administrator Guide
===================

The payment module is associated with CyberSource through the use of a
Secure Acceptance profile, and needs three pieces of data to identify the profile
and authenticate the requests sent to CyberSource. All of these are available to
be retrieved from the CyberSource Web site under
Tools & Settings -> Secure Acceptance -> Profiles

* Profile Id: The *Name* of the Profile as listed under Active Profiles.
Example: `Online Payments` (This should be called Profile Name, but this is part
of the CyberSource SPI).

Both of the following can be retrieved from a Security Key by clicking on the
`Security` link after clicking on an Active Profile. Clicking on a key will open
a dialog from which the values can be copied.

* Access Key: The public key for cryptography. This value will be exposed on the
payment form.

* Secret Key: The private key for cryptography. This value *should be kept secure
and not exposed publicly anywhere on the site*. Access to this key should be
restricted as much as possible. This is used by the backend code to
encrypt the communication with CyberSource and therefore verify that
the transaction is sanctioned to be processed against the merchant account.
If an attacker possesses this key they could submit fradulent transactions
against the account.

* Keep Settings...: This allows for more convenient manual updating of the
plugin when using the standard `KeyProvider` (which requires removal
of the existing plugin).

#### Security & Customizing Key Provision

The default implementation stores the keys in the database but this may raise
a concern if the site is not properly secured. Interactions with the admin side
of the site should only be done over https (preferably all parts of the site should
require https). Access to the underlying database should also be properly secured.
Encrypting the secret key at-rest is also worth considering in combination with
restricting access to the associated decryption mechanism.

To facilitate secure storage of the keys the key management is handled through a
very basic SPI mechanism consisting of the `KeyProvider` interface and the
`KeyProviderHolder`. To store the key in a more secure location (such as a
file not accessible anywhere through the interface), an implementation of
`KeyProvider` could be created and assigned to the
`KeyProviderHolder::$instance` reference where it will be used through the
rest of the plugin.


Author Guide
===========

In order to initiate the secured conversation with CyberSource
appropriate information must be attached to the requests. Several shortcodes
are provided which allow form to be embedded within content
to which fields can be added and which will properly submit the data to be
consumed by CyberSource.

These shortcodes are focused only on the functional aspects of communicating
with CyberSource rather than trying to provide any kind of complete widget
which would include any presentation concerns.  This is to keep the plugin
focused and (hopefully) lower maintenance while allowing maximal flexibility
for the author.

* `[slicypay_form]` is an enclosing shortcode to create the form which is used
to pass the information to CyberSource. This is the primary view container for
all related functionality.

* `[slicypay_field]` is a self-closing shortcode which should be used to generate
a form control within a `[slicypay_form]`. This has a required `name`
attribute. This is used to integrate the new field into the flow and make
sure that it is properly signed. Any other attributes will be output as
attributes within the generated `<input>` tag.
Presently only input elements are supported:
the type attribute can be provided but will default to `text`.

An example use of the shortcodes above is:
```html
<h1>Payment</h1>
[slicypaypay_form]
  <fieldset>
    <legend>Account information</legend>
    <ol>
      <li>
        <label for="amount">Amount To Pay</label>
        [slicypay_field name="amount"/]
      </li>
    </ol>
  </fieldset>
[/slicypay_form]
```
The original intent was that the field shortcode would generate tag attributes
only rather than complete elements, but this violates WordPress supported
behavior.

The JavaScript provided by the plugin is designed to handle signing the form
and the custom field behavior initially provided. It could be made more flexible
but presently there is no visible value in introducing the additional complexity.
If customization is desired the core functionality should likely be split from
any extensions, but even this is somewhat risky as one of the newer JavaScript
libraries may be in use on the site and should be leveraged for this functionality,
or the core functionality should be rewritten to avoid library use which would be
relatively time consuming.

The JavaScript has been left largely unmodified to avoid the additional overhead of
cross-browser testing. There may be some scripts which are no longer needed due
a combination of WordPress improving and older browsers being abandoned, but
as the cruft cannot removed quickly and safely it has been left as is.

Original Implementation
=======================

The original implementation of the payment module evolved from an earlier
approach based on the since terminated CyberSource Hosted Order Page solution,
and made several design choices to simplify integration while pursuing a consistent
feel. Some of the significant decisions will be outlined here to inform any
further implementations.

There are two immediate constraints when using the CyberSource hosted offering:
the limited ability to customize the hosted page and the restrictions inherent
when switching to a secured connection. These two factors led to the adoption of
using a second browser window and custom styled page to capture the additional
user information and send the user to CyberSource.

The second window allows for the creation of a consistent experience for the
payment submission without becoming tangled with the main experience of the site.
The smaller experience is then ended by an AJAX endpoint exposed in WordPress that
simply serves to close the second window.

If abandoning the second window is desired then the underlying constraints which
led to its adoption should be acknowledged: the page which collects the payment
window itself has limited customization options, and the secured connection from
the user to that page can not be interfered with. When using the hosted payment
page option there will be a divergent flow to some extend while communicating with
CyberSource.

If the original implementation is abandoned then the associated CSS and order form
generation code should be removed or appropriately marked as no longer used.
The CSS was largely copied from the CyberSource payment page to facilitate
creating a feel consistent with that page, and so is full of noise and cruft.

Disclaimer
==========

The author of this plugin (mwhipple) does not spend significant time working with
the technology stack which powers this plugin. Some conventional best practices
may be spotty due to a lack of an appropriate environment or readily available
knowledge which would have made adoption justifiable.
Style guides were not adhered to; the decision between spinal-case & snake_case
was only partly fleshed out (that seems an unfortunate aspect of WP) and
automated testing was not done (largely due to the large role of external
dependencies).

