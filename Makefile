BUILD_DIR = "build"

${BUILD_DIR}
	mkdir -p ${BUILD_DIR}

package: | ${BUILD_DIR}
	zip -r src ${BUILD_DIR}/slicypay.zip
